#!/bin/bash

if [ "$#" -ne 1 ]; then
	echo "Proper usage: $0 <key file>"
	exit
fi

if [ ! -f "$1" ]; then
	echo "Key file not found! Have you built the suite with make?"
	exit
fi

if [ `stat -c %s key` -ne 32 ]; then
	echo "Key not 32 bytes! Have you built the suite with make?"
	exit
fi

if [ ! -f sest.bf ]; then
	echo "sest.bf not found! Have you built the suite with make?"
	exit
fi

boop() {
	kill -9 $BOOB
	rm -r $POOP
	exit
}

POOP=$(mktemp -d)

mkfifo "$POOP/fif"

cat > "$POOP/fif" &

cat $1 > "$POOP/fif" &

deadbeef -z sest.bf < "$POOP/fif" &
BOOB=$!

trap boop SIGINT

cat /dev/stdin > "$POOP/fif"; boop